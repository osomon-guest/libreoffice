#!/bin/bash
# autopkgtest check: Run junit base tests against an installed version of LibreOffice
# (C) 2013 Canonical Ltd.
# (c) 2015-2017 Software in the Public Interest, Inc.
# Authors: Bjoern Michaelsen <bjoern.michaelsen@canonical.com>
#          Rene Engelhard <rene@debian.org>

set -e
set -E

SRCDIR=`pwd`
WORKDIR=`mktemp -d`
CHECK_PARALLELISM=1

function unapply() {
	echo "====== Unapplying the patch ======"
	patch -p1 -R < ./debian/tests/patches/java-subsequentcheck-standalone.diff
}

trap "unapply" ERR

echo "====== Patching the tree to only build Java-based unittests against an existing installation ======"
patch -p1 < ./debian/tests/patches/java-subsequentcheck-standalone.diff

echo
echo "====== Generating configuration ======="
rm -f config_host.mk
./debian/rules config_host.mk enable_report_builder=no
sed -i 's/export OOO_JUNIT_JAR=.*/export OOO_JUNIT_JAR=\/usr\/share\/java\/junit4.jar/' config_host.mk

echo
echo "====== Cleaning tree ======"
make clean

OOO_TEST_SOFFICE="${1:-path:/usr/lib/libreoffice/program/soffice}"

echo
echo "====== Enabling core dumps ======"
# yes, we want core dumps and stack traces
ulimit -c unlimited

echo
echo "====== Starting subsequentcheck with ${CHECK_PARALLELISM} job against ${OOO_TEST_SOFFICE} ======"

eval `grep PLATFORMID config_host.mk`

export PARALLELISM=$CHECK_PARALLELISM
make -rk \
    OOO_TEST_SOFFICE=${OOO_TEST_SOFFICE} \
    bridges_SELECTED_BRIDGE=gcc3_$PLATFORMID \
    subsequentcheck verbose=t

unapply

